# dobro-spa

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

markets.remote_market_id нужен только для логина, для вывода списка магазинов. Запоминать и записывать надо markets.id

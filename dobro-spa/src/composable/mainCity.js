import {computed} from 'vue'
import {useMainStore} from "@/store";

export default function mainCity(length) {
    const store = useMainStore();
    return computed(() => {
        // return 'Ваши магазины в г. ' + (store.selectedShopTitle || 'Санкт-Петербург');
        return  (length > 1 ? 'Магазины в г. ' : 'Магазин в г. ') + store.selectedCity;
    })
}
